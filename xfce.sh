# == SIMPLE KDE INSTALLATION == #
#part1
reflector --verbose -c GB -l 5 --sort rate --save /etc/pacman.d/mirrorlist
timedatectl set-ntp true
lsblk
echo "Enter the Drive: "
read drive
fdisk $drive
lsblk
echo "Enter the Linux Partition: "
read partition
mkfs.ext4 $partition
mount $partition /mnt
sed -i 's/^#Para/Para/' /etc/pacman.conf
pacstrap /mnt base linux linux-firmware intel-ucode
cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist
genfstab -U /mnt >> /mnt/etc/fstab
sed '1,/^#part2$/d' xfce.sh > /mnt/xfce2.sh
chmod +x /mnt/xfce2.sh
arch-chroot /mnt ./xfce2.sh
exit

#part2
ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
hwclock --systohc
echo "en_GB.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_GB.UTF-8" >/etc/locale.conf
echo "KEYMAP=us" > /etc/vconsole.conf
echo "Enter the Hostname: "
read hostname
echo $hostname > /etc/hostname
sed -i 's/^#Para/Para/' /etc/pacman.conf
pacman -S --noconfirm --needed noto-fonts
pacman -S --noconfirm --needed linux-headers xorg networkmanager firefox networkmanager grub sudo
pacman -S --noconfirm --needed xfce4 xfce4-goodies lightdm lightdm-gtk-greeter

read -p "do you want to install virtualbox guest? [y/n]" answer
if [[ $answer = y ]] ; then
  pacman -S --noconfirm --needed virtualbox-guest-utils
  systemctl enable vboxservice
fi

systemctl enable NetworkManager
systemctl enable lightdm
passwd
lsblk
echo "Enter grub drive: "
read gdrive
grub-install --recheck $gdrive
grub-mkconfig -o /boot/grub/grub.cfg
echo "%wheel ALL=(ALL) NOPASSWD: ALL " >> /etc/sudoers
echo "Enter Your Username: "
read username 
useradd -mG wheel $username
passwd $username
exit
